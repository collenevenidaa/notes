import * as React from 'react';
import { Link } from 'react-router-dom';
import Box from '@mui/material/Box';
import Drawer from '@mui/material/Drawer';
import IconButton from '@mui/material/IconButton';
import MenuIcon from '@mui/icons-material/Menu';
import List from '@mui/material/List';
import Divider from '@mui/material/Divider';
import ListItem from '@mui/material/ListItem';
import ListItemText from '@mui/material/ListItemText';
import { ListItemButton, Stack, ListItemIcon } from '@mui/material';
import HomeRoundedIcon from '@mui/icons-material/HomeRounded';
import AppRegistrationIcon from '@mui/icons-material/AppRegistration';
import EventNoteIcon from '@mui/icons-material/EventNote';

export default function Sidenav() {
  const [state, setState] = React.useState({
    left: false,
  });

  const menu = [
    {
      text: 'Home',
      to: '/',
      action: null,
      icon: (<HomeRoundedIcon />)
    },
    null,
    {
      text: 'Sign-up',
      to: '/sign-up',
      action: null,
      icon: (<AppRegistrationIcon />)
    },
    null,
  ];

  const toggleDrawer = (anchor, open) => (event) => {
    if (
      event.type === 'keydown' &&
      (event.key === 'Tab' || event.key === 'Shift')
    ) {
      return;
    }

    setState({ ...state, [anchor]: open });
  };

  const list = (anchor) => (
    <Box
      sx={{ width: anchor === 'top' || anchor === 'bottom' ? 'auto' : 250 }}
      role="presentation"
      onClick={toggleDrawer(anchor, false)}
      onKeyDown={toggleDrawer(anchor, false)}
    >
      <Stack
        sx={{ p: 2, bgcolor: 'primary.main' }}
        direction="row"
        justifyContent="center"
        alignItems="center"
      >
        <EventNoteIcon />
      </Stack>
      <List dense sx={{ p: 0 }}>
        {menu.map((menuItem, index) => {
          if (menuItem === null) {
            return (
              <div key={index}>
                <Divider></Divider>
              </div>
            );
          }

          const listItem = (
            <ListItemButton onClick={menuItem.action}>
              <ListItemIcon>
                {menuItem.icon}
              </ListItemIcon>
              <ListItemText primary={menuItem.text} />
            </ListItemButton>
          );

          return (
            <ListItem
              disablePadding
              button
              key={index}
              to={menuItem.to}
              component={Link}
            >
              {listItem}
            </ListItem>
          );
        })}
      </List>
      <Divider />
    </Box>
  );

  return (
    <div>
      {['left'].map((anchor) => (
        <React.Fragment key={anchor}>
          <IconButton
            onClick={toggleDrawer(anchor, true)}
            size="large"
            edge="start"
            color="inherit"
            aria-label="open drawer"
            sx={{ mr: 2 }}
          >
            <MenuIcon />
          </IconButton>
          <Drawer
            anchor={anchor}
            open={state[anchor]}
            onClose={toggleDrawer(anchor, false)}
          >
            {list(anchor)}
          </Drawer>
        </React.Fragment>
      ))}
    </div>
  );
}
