import * as React from 'react';
import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField';

const Signup = (props) => {

  const signupDetails = {
    username: ""
  }

  const [signup, setSignup] = React.useState(signupDetails);

  const handleChange = (event) => {
    const value = event.target.value;
    setSignup({...signup, [event.target.name]: value });
  };

  return (
    <Box>
       <TextField 
          name='username'
          label="Username" 
          variant="outlined" 
          autoFocus
          margin="dense"
          type="text"
          fullWidth
          onChange={handleChange}
          value={signup?.username} 
        />
    </Box>
  );
};

export default Signup;
