import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';

import SideNav from './Sidenav';


const Navbar = (props) => {
  
  return (
    <Box sx={{ flexGrow: 1 }}>
    <AppBar color='primary'>
      <Toolbar> 
        <SideNav /> 
        <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
          Notes
        </Typography>
      </Toolbar>
    </AppBar>
  </Box>
  );
};

export default Navbar;
