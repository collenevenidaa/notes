import { useState } from 'react';
import './App.css';
import { BrowserRouter as Router } from 'react-router-dom';
import Routes from './routes/router.jsx';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import NavBar from './components/NavBar.jsx';


import { lime, purple } from '@mui/material/colors';

const theme = createTheme({
  palette: {
    primary: lime,
      secondary: purple,
    },
  });
function App() {
  const [count, setCount] = useState(0)

  return (
    <ThemeProvider theme={theme}>
      <NavBar />

      <Router>
        <Routes />
      </Router>

    </ThemeProvider>
  )
}

export default App
