import * as React from 'react'
import { Switch, Route } from "react-router-dom";

import Navbar from '../components/Navbar';
import Home from '../components/Home';
import Signup from '../components/Signup';

const Routes = () => {

  return (
    <div>
      <Navbar />
      <Switch>
        <Route exact path="/" component={Home} />
        <Route exact path="/sign-up" component={Signup} />
      </Switch>
    </div>
  )
}

export default Routes
